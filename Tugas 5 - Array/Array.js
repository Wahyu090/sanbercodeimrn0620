//Soal no 1

console.log("\nSoal No 1\n")

var Hasil = []
function range (num1, num2){
    if (num1 == undefined || num2 == undefined){
        Hasil.push(-1)
        return Hasil
    }
    else if (num1 < num2){
        for (var i = num1 ; i<= num2 ; i++){
            Hasil.push(i)
        }
        return Hasil
    }
    else if (num1 > num2){
        for(var i = num1 ; i>= num2; i--){
            Hasil.push(i)
        }
        return Hasil
    }
}

console.log(range(1, 10))
//console.log(range(1))
//console.log(range(11, 18))
//console.log(range(54, 50))
//console.log(range())

console.log ("\n----------------------------------------------------------------\n")


//Soal no 2
/*
console.log("\nSoal No 2\n")

var Hasil = []
function rangeWithStep (num1, num2, step){
    if (num1 == undefined || num2 == undefined){
        Hasil.push(-1)
        return Hasil
    }
    else if (num1 < num2){
        for (var i = num1 ; i<= num2 ; i+=step){
            Hasil.push(i)
        }
        return Hasil
    }
    else if (num1 > num2){
        for(var i = num1 ; i>= num2; i-=step){
            Hasil.push(i)
        }
        return Hasil
    }
}
//console.log(rangeWithStep(1, 10, 2))
//console.log(rangeWithStep(11, 23, 3))
//console.log(rangeWithStep(5, 2, 1))
//console.log(rangeWithStep(29, 2, 4))

console.log ("\n----------------------------------------------------------------\n")
*/

//Soal no 3
/*
console.log("\nSoal No 3\n")
function sum(num1, num2, step){
    var total = 0
    var arr
    var i
    if (!num1 && !num2 && !step){total = 0}
    else if (num1 && !num2 && !step){total = num1}
    else {
        if (!step){arr=rangeWithStep (num1, num2, 1)}
        else {
            arr=rangeWithStep (num1, num2, step)
            }
        for (i=0;i<arr.length;i++){
            total= total +parseInt(arr[i])
        }
    }
    return total
}

//console.log(sum(1, 10))
//console.log(sum(5, 50, 2))
//console.log(sum(15, 10))
//console.log(sum(20, 10, 2))
//console.log(sum(1))
//console.log(sum())

console.log ("\n----------------------------------------------------------------\n")
*/

//Soal no 4
/*
console.log("\nSoal No 4\n")
var Input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/04/1970", "Berkebun"],
]
function dataHandling (Bio){
    for (var i = 0; i < Bio.length; i++){
        console.log("Nomor ID: " + Bio [i][0])
        console.log("Nama Lengkap: " + Bio [i][1])
        console.log("TTL: " + Bio [i][2] + ", " + Bio [i][3])
        console.log("Hobi: " + Bio [i][4])
        console.log('\n')
    }

}
dataHandling(Input)

console.log ("\n----------------------------------------------------------------\n")
*/



//Soal No 5
/*
console.log("\nSoal No 5\n")

function balikKata(kata){
    var i
    var Hasil = ""
    for(i = kata.length-1; i >= 0; i--){
        Hasil += kata[i]
    }
      return Hasil   
}

//console.log(balikKata("Kasur Rusak"))
//console.log(balikKata("Sanbercode"))
//console.log(balikKata("Haji Ijah"))
//console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

console.log ("\n----------------------------------------------------------------\n")
*/

//Soal No 6

console.log("\nSoal No 6\n")

function dataHandling2 (bio){
    var Tanggal
    var Bulan
    var NamaBulan

    //Splice
    bio.splice (1, 1, bio[1]+" Elsharawy")
    bio.splice (2, 1, "Provinsi " + bio[2])
    bio.splice (4, 1, "Pria", "SMA Internasional Metro")
    console.log (bio)


    //split
    var Tanggal = bio[3].split("/")
    var Bulan = parseInt(Tanggal[1])
    switch (Bulan){
        case 1: {(NamaBulan = "Januari");break;}
        case 2: {(NamaBulan = "Pebruari");break;}
        case 3: {(NamaBulan = "Maret");break;}
        case 4: {(NamaBulan = "April");break;}
        case 5: {(NamaBulan = "Mei");break;}
        case 6: {(NamaBulan = "Juni");break;}
        case 7: {(NamaBulan = "Juli");break;}
        case 8: {(NamaBulan = "Agustus");break;}
        case 9: {(NamaBulan = "September");break;}
        case 10: {(NamaBulan = "Oktober");break;}
        case 11: {(NamaBulan = "November");break;}
        case 12: {(NamaBulan = "Desember");break;}
        default: {"Input Bulan Salah"}
    }
    console.log(NamaBulan)

    //Sort Descending
    Tanggal.sort(function(a, b){return b - a})
    console.log(Tanggal)

    //Join
    var Tanggal2 = bio[3].split("/").join("-")
    console.log(Tanggal2)

    //Slice
    var stringnama = bio[1].slice(0, 14)
    console.log(stringnama)
}

var Input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2 (Input)




