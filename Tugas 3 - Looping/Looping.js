//Soal no 1
console.log('Looping Pertama');
var angka = 1
while(angka<=20){
    if(angka%2==0) {
        console.log(angka + " - " + "I love coding");
    }
    angka++;
}

console.log('\nLooping Kedua');
var angka2 = 20
while(angka>1){
    if(angka%2==0){
        console.log(angka + " - " + "I love coding");
    }
    angka--;
}

console.log("\n-------------------------------------------------\n")

//Soal no 2
console.log('\nOUTPUT');
var i = 1
for (var i=1;i<=20;i++){
    if (i%2==0){
        console.log(i + " - " + "Berkualitas"); 
    }
    else if (i%3==0 && i%2==1){
        console.log(i + " - " + "I Love Coding");
    }
    else if (i%2==1){
        console.log(i + " - " + "Santai");
    }
    else ('Nilai Eror');
}

console.log("\n-------------------------------------------------\n")


//Soal no 3

var a = ''
for (var i = 1; i < 5 ; i++){
    for (var j = 1; j < 9; j++){
        a += '#'
    }
    a += '\n'
}
console.log(a);

console.log("\n-------------------------------------------------\n");

//Soal no 4
var b = ''
for (var i=1;i<8;i++){
    for(var j=1;j<=i;j++){
        b += '#'
    }
    b += '\n'
}
console.log(b);


    


console.log("\n-------------------------------------------------\n");

//Soal no 5
var a = ''
for (var i =1; i <= 8; i++){
    if (i%2==0) {
        for(var j = 1; j<=4;j++){
            a += "# "
        }
    }
    else if (i%2==1) {
        for (var j=1;j<=4;j++){
            a += " #"
        }
    }
    a += "\n"
}
console.log(a);
