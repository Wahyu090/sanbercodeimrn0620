var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

var i = 0
function panggil (times){
    if(i<books.length){
        readBooksPromise (times, books[i])
        .then(hasil=> panggil (hasil))
        .catch(error=>console.log(error))
    }
    i++;
}
panggil (10000)