// If - Else
var nama = 'wahyu'
var peran = 'Penyihir'
if (nama == '' && peran == '') {console.log('Nama harus diisi!')}
    else if (nama != '' && peran == '') {console.log('Halo ' + nama + ', Pilih peranmu untuk memulai game!')}
    else if (nama != '' && peran == 'Penyihir'){console.log('Selamat datang di Dunia Werewolf, ' + nama + ' \nHalo Penyihir ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!')}
    else if (nama != '' && peran == 'Guard'){console.log('Selamat datang di Dunia Werewolf, ' + nama + ' \nHalo Guard ' + nama + ', kamu dapat membantu melindungi temanmu dari serangan werewolf!')}
    else if (nama !='' && peran == 'Werewolf'){console.log('Selamat datang di Dunia Werewolf, ' + nama + ' \nHalo Werewolf ' + nama + ', Kamu akan memakan mangsa setiap malam!')}
else {console.log('Peran Tidak Sesuai')}

// Switch Case
var Tanggal = 5;
var Tahun = 1945;
var Bulan = 8;

switch (true) 
{
  case (Tanggal < 1 || Tanggal > 31):
    console.log('Input Tanggal Salah')
    break;
  case (Tahun < 1900 || Tahun > 2200):
    console.log('Input Tahun Salah')
    break;
  case (Bulan < 1 || Bulan > 12):
    console.log('Input Bulan Salah')
    break;
  default:
    switch (Bulan) 
    {
      case 1:
        TextBulan='Januari';
        console.log(Tanggal + " " + TextBulan + " " + Tahun);
        break;
      case 2:
        TextBulan='Pebruari';
        console.log(Tanggal + " " + TextBulan + " " + Tahun);
        break;
      case 3:
        TextBulan='Maret';
        console.log(Tanggal + " " + TextBulan + " " + Tahun);
        break;
      case 4:
        TextBulan='April';
        console.log(Tanggal + " " + TextBulan + " " + Tahun);
        break;
      case 5:
        TextBulan='Mei';
        console.log(Tanggal + " " + TextBulan + " " + Tahun);
        break;
      case 6:
        TextBulan='Juni';
        console.log(Tanggal + " " + TextBulan + " " + Tahun);
        break;
      case 7:
        TextBulan='Juli';
        console.log(Tanggal + " " + TextBulan + " " + Tahun);
        break;
      case 8:
        TextBulan='Agustus';
        console.log(Tanggal + " " + TextBulan + " " + Tahun);
        break;
      case 9:
        TextBulan='September';
        console.log(Tanggal + " " + TextBulan + " " + Tahun);
        break;
      case 10:
        TextBulan='Oktober';
        console.log(Tanggal + " " + TextBulan + " " + Tahun);
        break;
      case 11:
        TextBulan='November';
        console.log(Tanggal + " " + TextBulan + " " + Tahun);
        break;
      case 12:
        TextBulan='Desember';
        console.log(Tanggal + " " + TextBulan + " " + Tahun);
        break;
      default:
        console.log('Input Bulan Salah')
        break;
    }
    break;
}