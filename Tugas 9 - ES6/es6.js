//Soal No 1
console.log("Soal No. 1\n")
const golden = goldenfunction = () => {console.log("this is golden!!")};

golden ()

console.log("\n ---------------------------------------------------------------------------- \n")

//Soal No 2
console.log("Soal No. 2\n")
const newFunction = (firstName, lastName) => {return {
    firstName : firstName,
    lastName : lastName,
    fullName : () => {
        console.log(firstName + " " + lastName)
        return
    }
}
}

newFunction("William", "Imoh").fullName()

console.log("\n ---------------------------------------------------------------------------- \n")

//Soal No 3
console.log("Soal No. 3\n")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation} = newObject;

console.log(firstName, lastName, destination, occupation);

console.log("\n ---------------------------------------------------------------------------- \n")

//Soal No 4
console.log("Soal No. 4\n")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]
console.log(combined);

console.log("\n ---------------------------------------------------------------------------- \n")

//Soal No 5
console.log("Soal No. 5\n")
const planet = "earth"
const view = "glass"

const before = `lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(before);

console.log("\n ---------------------------------------------------------------------------- \n")