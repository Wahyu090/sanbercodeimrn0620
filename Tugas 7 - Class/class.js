//Soal No. 1
console.log("Soal No. 1\n")

class Animal{
    constructor(name){
        this.name1 = name;
        this.legs1 = 4;
        this.cold_blooded1 = false;
    }

get name (){return this.name1;}
get legs (){return this.legs1;}
get cold_blooded (){return this.cold_blooded1;}
}

 class Ape extends Animal{
     constructor(name1, cold_blooded1){
         super(name1, cold_blooded1);
         this.legs1 = 2;
     }
     yell (){return "Auooo";}
 }

 class Frog extends Animal{
     constructor(name1, legs1, cold_blooded1){
         super(name1, legs1, cold_blooded1);
     }
     jump() {return "hop hop";}
 }


var sheep = new Animal("shaun");
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

var sungokong = new Ape ("kera sakti")
console.log(sungokong.yell())

var kodok = new Frog ("buduk")
console.log(kodok.jump())

console.log('\n---------------------------------------------------\n')

//Soal No. 2
console.log("Soal No. 2\n")

class Clock{
    constructor ({template}){
        this.template1 = template;
        this.timer = "";
    }
    render(){
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = "0" + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = "0" + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = "0" + secs;

        var output = this.template1
        .replace("h", hours)
        .replace("m", mins)
        .replace("s", secs);

        console.log(output);
    }
    Stop(){
        clearInterval(this.timer);
    };

    Start(){
        this.timer = setInterval(() => this.render(), 1000);
    };
}

var clock = new Clock ({template: "h:m:s"});
clock.Start()

