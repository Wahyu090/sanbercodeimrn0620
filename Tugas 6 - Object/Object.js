// Soal No.1
console.log ("Saol No 1\n")

var now = new Date()
var thisYear = now.getFullYear()
var tahunInt
var tahunString
var age
var namaLengkap = {}
function arrayToObject(arr){
    if(arr[0]==null){
        console.log('" "')
    }
    else {
        for (var i = 0; i < arr.length; i++){
            if (arr[i][3]== null || arr[i][3]== "undefined" || arr[i][3]> thisYear){
                tahunString = "Invalid Birth Year"
            }
            else {
                tahunInt =  thisYear - arr[i][3]
                tahunString = tahunInt
            }
            var input = {}
            input.firstName = arr[i][0]
            input.lastName = arr[i][1]
            input.gender = arr[i][2]
            input.age = tahunString
            namaLengkap = input
            console.log(`${i+1}. ${arr[i][0]} ${arr[i][1]}: `)
            console.log(namaLengkap)
            namaLengkap = {}
            input = {}
        }
    }
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)

console.log ("\n -------------------------------------------------------------------------- \n")

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)

console.log ("\n -------------------------------------------------------------------------- \n")

//Soal No.2
console.log("Soal No 2\n")

var listBarang =[["Sepatu brand Stacattu", 1500000], ['Baju brand Zoro', 500000], ["Baju brand H&N", 250000], ["Sweater brand Uniklooh", 175000], ["Casing Handphone", 50000]]
function shoppingTime (memberId, money){
    if (!memberId){return "Mohon maaf, toko X hanya berlaku untuk member saja"}
    else if (money < 50000){return "Mohon maaf, uang tidak cukup"}
    var belanja={
        memberId : memberId,
        money : money,
        listPurchased: [],
        changeMoney : null,
    }
    for (var i = 0; i < listBarang.length; i++){
        if(money >= listBarang[i][1]){
            belanja.listPurchased.push(listBarang[i][0])
            money -= listBarang [i][1]
            
        }
    }
    belanja.changeMoney = money
    return belanja
}
console.log(shoppingTime("1820RzKrnWn08", 2475000))
console.log(shoppingTime("82Ku8Ma742", 170000))
console.log(shoppingTime("", 2475000))
console.log(shoppingTime('234JdhweRxa53', 15000))
console.log(shoppingTime())

console.log("\n --------------------------------------------------------------------\n")

//Soal No. 3
console.log("Soal No 3\n")

function naikAngkot(arrPenumpang){
   var rute = ['A', 'B', 'C', 'D', 'E', 'F']
   var hasil = []
   var obj = {}
   for (var i = 0; i < arrPenumpang.length; i++){
       obj.penumpang = arrPenumpang[i][0];
       obj.naikDari = arrPenumpang[i][1];
       obj.tujuan = arrPenumpang[i][2];
       obj.bayar = (2000*(rute.indexOf(arrPenumpang[i][2])-rute.indexOf(arrPenumpang[i][1])));
       hasil.push(obj)
       obj = {}
   }
   return hasil 
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));

